import os
import csv
import json

currentPath = os.getcwd()


def csv_to_json():
    data_dict = []

    with open(currentPath + '/python/negativos.csv', 'r', encoding='utf-8') as csv_file_handler:
        csv_reader = csv.DictReader(csv_file_handler)

        for row in csv_reader:
            data_dict.append(get_keys(row))

    print_to_json(data_dict)


def get_keys(row):
    current_row = {}
    for attr in row:
        current_row[attr] = row[attr]
    return current_row


def print_to_json(data_dict):
    with open(currentPath + '/src/assets/negativos.json', 'w', encoding='utf-8') as json_file_handler:
        json_file_handler.write(json.dumps(data_dict, indent=4, ensure_ascii=False))
    print('done!')


csv_to_json()
