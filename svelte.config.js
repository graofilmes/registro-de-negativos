import preprocess from "svelte-preprocess"

/** @type {import('@svelte/kit').Config} */
const config = {
    preprocess: preprocess()
}

export default config
