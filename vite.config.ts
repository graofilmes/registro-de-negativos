import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte()],
  publicDir: 'static',
  build: {
    outDir: 'build'
  },
  base: process.env.NODE_ENV === 'development' ? '' : '/registro-de-negativos/'
})
